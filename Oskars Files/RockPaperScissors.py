#Copyright 2018 Oskar Ohman, Alexander Enlund

#Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
#and associated documentation files (the "Software"), to deal in the Software without restriction, 
#including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
#and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


from random import randint

def checkResults(playerChoice,computerChoice):
    if playerChoice == computerChoice:
        print "It's a tie"
    elif playerChoice == 1 and computerChoice == 3 or playerChoice == 2 and computerChoice == 1 or playerChoice == 3 and computerChoice == 2:
        # returns true if player wins
        return True
    else:
        # returns false if computer wins
        return False

# loops untill player enters a number greater than 0        
while True:
    try:
        scorelimit = int(input("How many rounds do you wish to play to?: "))
        if scorelimit <= 0:
            raise ValueError
        break
    except:
        print "Please use only numbers greater than 0"

# the tuple rps is used to when printing out what the player and computer chose
rps = ("rock", "paper", "scissors")
playerScore = 0
computerScore = 0

while True:
    # breaks loop once scorelimit is reached
    if playerScore >= scorelimit or computerScore >= scorelimit:
        break
    
    # loops until player inputs either 1, 2 or 3
    while True:
        try:
            playerChoice = input("\nChoose\n1 for rock\n2 for paper\n3 for scissors\n> ")
            
            if playerChoice in (1,2,3):
                break
            else:
                raise ValueError
        except:
            print "Please enter 1, 2 or 3\n"

    computerChoice = randint(1,3)
    
    print "\n\n\n\n\n\n\n\nPlayer chose", rps[playerChoice-1]
    print "Computer chose", rps[computerChoice-1]
    
    play = checkResults(playerChoice, computerChoice)
    
    if play == True:
        playerScore += 1
        print "Player won!"
    elif play == False:
        computerScore += 1
        print "Computer won!"
    
print "--------------------\nPlayer score: %s\nComputer score: %s" % (playerScore, computerScore)

if playerScore > computerScore:
    print "Player wins!"
elif playerScore < computerScore:
    print "Computer wins!"
else:
    print "ERROR, something went wrong"