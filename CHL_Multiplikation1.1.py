# coding=UTF-8
#importerar UTF-8 teckenkodning för åäö-support

import random
#impoterar random-modulen för slumpmässiga tal


print "Välkommen till multiplikationstestet!\nHär testar vi dina kunskaper!\n"
#välkomstmeddelande
while True:
    try:
        difficulty = int(input("Välj svårighetsgrad:\n" \
              "1. Multiplikationstabell 0 till 10\n" \
              "2. Multiplikationstabell 0 till 10 med negativa tal\n" \
              "3. Multiplikationstabell 0 till 50\n" \
              "4. Multiplikationstabell 0 till 50 med negativa tal\n" \
              "5. Multiplikationstabell 0 till 100\n"\
              "6. Multiplikationstabell 0 till 100 med negativa tal\n"))
        #ger användaren några svårighetsalternativ
        
        if difficulty < 1 or difficulty > 6:
            raise ValueError
        # om användaren matar in siffra mindre än 1 eller större än 6 ser vi det som ett error
        break
    except:
        print "Något gick fel, mata in ett tal mellan 1-6 i siffror!\n\n"
        # felhantering om användaren matar in något annat än siffor



def multiplikation():
    userScore = 0
    #nollställer poängen före vi börjar ställa frågor
    while True:
        try:
            antalQuestion = int(input("Hur många frågor vill du svara på?\n"))
            #frågar hur många frågor användaren vill svara på
            break
        except:
            print "Något gick fel, mata endast in ett tal i siffror!\n"
        #felhantering om användaren matar in något annat än siffor

    for question in range(1,antalQuestion+1,1):
        #for loopar för det givna antalet frågor
        if difficulty == 1:
            multiplikator = random.randint(0,10)
            #genererar en multiplikator slumpmässigt

            multiplikand = random.randint(0,10)
            #genererar en multiplikand slumpmässigt

        # om användaren valt svårighetsgrad 1


        elif difficulty == 2:
            multiplikator = random.randint(-10,10)
            multiplikand = random.randint(-10,10)
        # om användaren valt svårighetsgrad 2

        elif difficulty == 3:
            multiplikator = random.randint(0,50)
            multiplikand = random.randint(0,50)
        # om användaren valt svårighetsgrad 3

        elif difficulty == 4:
            multiplikator = random.randint(-50,50)
            multiplikand = random.randint(-50,50)
        # om användaren valt svårighetsgrad 4

        elif difficulty == 5:
            multiplikator = random.randint(0,100)
            multiplikand = random.randint(0,100)
        # om användaren valt svårighetsgrad 5

        elif difficulty == 6:
            multiplikator = random.randint(-100,100)
            multiplikand = random.randint(-100,100)
        #om användaren valt svårighetsgrad 6

        produkt = multiplikator * multiplikand
        #kalkylerar det korrekta svaret

        query = "Vad är " + str(multiplikator) + " * " + str(multiplikand) + "?\nDitt svar: "
        #skapar en sträng med variablerna multiplikator och multiplikand för att begära användarens svar
        while True:
            try:
                userProdukt = int(input(query))
                #begär användarens svar
                break
            except:
                print "Något gick fel, mata endast in ett tal i siffror!\n"

        if userProdukt == produkt:
            print "Rätt!", multiplikator,"*",multiplikand,"är lika med", str(produkt) + "!\n\n"
            userScore +=1
        #om användaren svarar rätt, berättar vi att denne har svarat rätt och kommer ihåg att den svarat rätt

        else:
            print "Tyvärr", multiplikator,"*",multiplikand,"är lika med", str(produkt) + "...\n\n"
        #annars om användaren svarar fel informerar vi detta


    print "Du svarade rätt på", userScore, "av", antalQuestion, "frågor!"
    #vi berättar för användaren hur många rätta svar denne lyckats få

    vitsordProcent = float(userScore) / float(antalQuestion) * 100
    #räknar ut ett vitsord

    if vitsordProcent < 50:
        print "Du får vitsordet 4. Du är tyvärr underkänd!"
    elif vitsordProcent >= 50 and vitsordProcent < 60:
        print "Du får vitsordet 5. Du klarade dig denhär gången, men plugga hårdare nästa gång!"
    elif vitsordProcent >= 60 and vitsordProcent < 70:
        print "Du får vitsordet 6. Jag skulle be dig satsa hårdare, men det viktiga är att du själv är nöjd."
    elif vitsordProcent >= 70 and vitsordProcent < 80:
        print "Du får vitsordet 7. Bra jobbat!"
    elif vitsordProcent >= 80 and vitsordProcent < 90:
        print "Du får vitsordet 8. Fint!"
    elif vitsordProcent >= 90 and vitsordProcent < 100:
        print "Du får vitsordet 9. Vackert! Fortsätt i samma stil!"
    elif vitsordProcent == 100:
        print "Du får vitsordet 10! Utmärkt! Vem vet, du kanske är nästa Pythagoras?"
    #skriver ut olika vitsord och ger en kort kommentar

multiplikation()