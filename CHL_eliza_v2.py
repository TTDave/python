# encoding=UTF-8
"""
 eliza.py


 En verkligt simpel version av ett Eliza-liknande program
 1) Önskar användaren välkommen
 2) Upprepar följande i all oändlighet
    2.1)  Väntar på att användaren "säger" något
    2.2)  Svarar på ett sätt som verkar intelligent


 Svaret väljs alltid slumpmässigt ur en samling uttryck som vi lagrat i en lista. På detta sätt får vi användaren att tro
 att Eliza faktiskt är smart. Eller så inte :)
 """

import string
import random
import loggning
import time

positiva = ["Berätta mer",
            "Jag förstår...",
            "Ahaa...",
            "Jag lyssnar...",
            "Okej.",
            "Intressant.",
            "Tycker du det?",
            "Jaså...",
            "Jaha ja...",
            "Är det så du känner?"]

negativa = ["Varför är du på så dåligt humör?",
            "Är allting okej?",
            "Vill du prata om saken?",
            "Varför är du så negativt inställd?",
            "Varför inte?"]

familj = ["Men hur är det med din pappa då?",
          "Är din bror likadan?",
          "Hur är din relation till din mamma?",
          "Kom du bra överens med dina far- och morföräldrar?",
          "Men din syster, då?"]

def svara(text):

    # Kollar om sista index är någon av följande symboler: "! ? . , : ;"
    if text[(len(text)-1)] == ("!") or \
       text[(len(text) - 1)] == ("?") or \
       text[(len(text) - 1)] == (".") or \
       text[(len(text) - 1)] == (",") or \
       text[(len(text) - 1)] == (":") or \
       text[(len(text) - 1)] == (";"):
       # tar bort sista index om ovanstående stämmer
        text = text[:-1]

    # Dela upp användarens mening i en lista av ord
    urspr_ord = string.split(text)

    # Skapa en ny lista som innehåller samma ord (en kopia)
    nya_ord = urspr_ord[:]

    # Gå igenom alla ord, ett åt gången. Om ordet är i första person, så byt ut det till
    # motsvarande ord i andra person
    for i in range(len(urspr_ord)):
        if urspr_ord[i] == "jag":
            nya_ord[i] = "du"
        elif urspr_ord[i] == "min":
            nya_ord[i] = "din"
        elif urspr_ord[i] == "mitt":
            nya_ord[i] = "ditt"
        elif urspr_ord[i] == "mig":
            nya_ord[i] = "dig"
        elif urspr_ord[i] == "mina":
            nya_ord[i] = "dina"
        if urspr_ord[i] == "du":
            nya_ord[i] = "jag"
        elif urspr_ord[i] == "din":
            nya_ord[i] = "min"
        elif urspr_ord[i] == "ditt":
            nya_ord[i] = "mitt"
        elif urspr_ord[i] == "dig":
            nya_ord[i] = "mig"
        elif urspr_ord[i] == "dina":
            nya_ord[i] = "mina"

    # Om användaren verkar ha negativ avsikt
    if ("nej" in urspr_ord) or ("aldrig" in urspr_ord) or ("nä" in urspr_ord):
        # skriv ut ett listat negativt uttryck
        svar = random.choice(negativa)
        print svar
        loggning.log(svar)

    # Om användarens avsikt inte riktigt hittas (lite som en catch-all)
    elif nya_ord == urspr_ord:
        # skriv ut ett positivt listat uttryck
        svar = random.choice(positiva)
        print svar
        loggning.log(svar)

    # Om användaren talar om någon familjemedlem
    elif ("mamma" in urspr_ord) or ("pappa" in urspr_ord) or ("mor" in urspr_ord) or ("far" in urspr_ord) or \
         ("syster" in urspr_ord) or ("bror" in urspr_ord) or ("farmor" in urspr_ord) or ("farfar" in urspr_ord) or \
         ("mormor" in urspr_ord) or ("morfar" in urspr_ord):
        # skriv ut ett uttryck som relaterar till familjen
        svar = random.choice(familj)
        print svar
        loggning.log(svar)

    # Om användaren nämnt orden "du", "jag", "min", "din", etc...
    else:
        # slå ihop alla ord i nya_ord till en sträng och skriv ut den
        svar = string.join(nya_ord)
        svar += "?"
        print svar
        loggning.log(svar)

def main():
    # Loggar datum och tid för besöket
    loggning.log(time.ctime())

    # Välkomstmeddelande
    welcome = "**************************************************\n" \
          "*                                                *\n" \
          "*       Välkommen till Elizas mottagning         *\n" \
          "*                                                *\n" \
          "**************************************************\n" \
          "\n" \
          "(Du kan sluta när som helst genom att svara \"Hejs svejs\")\n" \
          "\n" \
          "'Berätta för mig om dina problem...'"
    print welcome
    loggning.log(welcome)


    # Fortsätt diskussionen i all oändlighet
    while True:

        # Vänta på att användaren matar in något
        text = raw_input("\n> ")
        text = string.lower(text)
        loggning.log(text)

        # Om avslutaren verkar vilja avsluta samtalet, avsluta
        if (text == "hejdå") or (text == "sluta") or (text == "hejs svejs") or (text == "ok, that does it"):
            break

        # Annars, påbörja svarsfunktionen
        else:
            svara(text)

    # Avslutningsfras
    svar = 'Tack för besöket. Betala in 150 EUR på mitt konto.'
    print svar
    loggning.log(svar)

# Påbörja huvudfunktionen
main()