# Python 3

# a simple script to run no steam games or other exe files 
# without the need to install and setup wine (dont except it to work with all games or any other file for that matter)
import os, sys
# The to get the compdata folder you must install a windows only game on linux to get the compdata files
# Test with diffrent compdata from diffrent game if it doesnt work cause some games have diffrent proton
# configurations
os.environ["STEAM_COMPAT_DATA_PATH"] =  "/path/to/SteamLibraryLinux/steamapps/compatdata/402570"

run = "run"
exe = "/path/to/exe/file.exe" # Remember to do correct formatting if file name contains whitespaces
                                    # like below forwardshlash and whitespace for whitespace
                                    # If this doesnt work maby proton has allready been defaulted 3.16 or any newer version for that matter
cmd = "/home/oskar/.local/share/Steam/steamapps/common/Proton\ 3.7/ " + run + " " + exe

for arg in sys.argv[1:]:
    cmd += " " + arg

os.system(cmd)
